An informal history of the module:

Version 1.0 of the module implemented only a DAP client. The client
downloaded data in ASCII format, and worked only with Arrays and
Grids. It was developed by reverse-engineering the responses from
the official DAP server. It really should have been released as
0.0.1, but I'm good at marketing.

Further 1.x versions brought binary data transfer using Python's
xdrlib module, a DDS/DAS parser that wasn't based on regular
expressions, a simple server architecture based on plugins and a
common core that could run as CGI, with BaseHTTPServer or Twisted.

Version 2.0 was a rewrite from scratch, after extensively reading
the DAP 2.0 specification draft. The DDS/DAS parser was written by
hand, a faster xdrlib module was designed, and the server was built
based on the WSGI specification. The client and the server were
able to handle almost *any* dataset structure, metadata in the DAS,
and much more. This version was supported by Google during the 2005
Summer of Code, and supervised by Paul Dubois.

Version 2.1 implemented a fully buffered server, from the plugin
layer to the HTTP mechanism, allowing datasets of infinite size to
be served. The module moved from distutils to setuptools, plugins
got "smarter" and a THREDDS catalog WSGI middleware generator was
built. The server was also Paste-deployified.

In version 2.2 a lot of code was rewriten. The code was cleaned up
and made more modular. The client got a few improvements, with
support for authentication, cache, and a more pythonic handling of
sequences. The server was streamlined and became much more fast.

Version 2.3 brought indexable sequences (both on client and server
side), and a handy rewrite of some legacy code from 2.0 (xdr.py and
proxy.py specially). This release was motivated by the Dapper
specification for in-situ data. A lot of parallel work on pupynere,
arrayterator, a thredds WSGI app and plugins (HDF5, GrADS).
