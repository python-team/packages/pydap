This is a Python implementation of the Data Access Protocol, a
scientific protocol for data access developed by the OPeNDAP team
(http://opendap.org). This implementation is developed from scratch,
following the latest specification of the protocol (DAP 2.0 Draft
Community Standard 2005/04/27) and based on my experience with
OPeNDAP servers on the wild.

Using this module one can access hundreds of scientific datasets
from Python programs, accessing data in an efficient, transparent
and pythonic way. Arrays are manipulated like normal multi-dimensional
arrays (like numpy.array, e.g.), with the fundamental difference
that data is downloaded on-the-fly when a variable is sliced.
Sequential data can be filtered on the server side before being
downloaded, saving bandwith and time.

The module also implements a DAP server, allowing datasets from a
multitude of formats (netCDF, Matlab, CSV files, SQL RDBMS) to be
served on the internet. The server specifies a plugin API for
supporting new data formats in an easy way. The DAP server is
implemented as a WSGI application (see PEP 333), running on a variery
of servers, and can be combined with WSGI middleware to support
authentication, gzip compression and much more.

For more information, please check the official website (http://pydap.org)
or the included documentation.

This module was supported in 2005 by the Google Summer of Code,
mentored by Paul Dubois of Numeric Python fame, and a few donations
through PayPal. I also received a grant from CNPq to implement the
server at CPTEC/INPE, giving me some generous time to work on the
module while being paid at the same time (to think that it all
started from fun!). Thanks for everyone that helped me with this
project.

(c) 2003-2007 Roberto De Almeida <rob@pydap.org>
http://pydap.org/
