"""A Python implementation of the Data Access Protocol (DAP).

Pydap is a Python module implementing the Data Access Protocol (DAP)
written from scratch. The module implements a DAP client, allowing
transparent and efficient access to dataset stored in DAP server, and
also implements a DAP server able to serve data from a variety of
formats.

For more information about the protocol, please check http://opendap.org.
"""

__import__('pkg_resources').declare_namespace(__name__)
